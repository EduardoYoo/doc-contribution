# Arrays of bytes
import std::clone::Clone
import std::cmp::(Contains, Equal)
import std::drop::Drop
import std::fmt::(Format, Formatter)
import std::hash::(Hash, Hasher)
import std::index::(bounds_check, Index, SetIndex)
import std::iter::(Enum, Iter)
import std::option::Option
import std::string::(IntoString, ToString)

# A type from which a new `ByteArray` can be created.
trait pub ToByteArray {
  fn pub to_byte_array -> ByteArray
}

# A type which can be moved into a `ByteArray`.
trait pub IntoByteArray {
  fn pub move into_byte_array -> ByteArray
}

# An array of bytes.
#
# Byte arrays are arrays specialised for storing individual bytes in the most
# efficient way possible. Unlike a regular `Array` of `Int` values, each value
# only requires a single byte of space, instead of requiring 8 bytes of space.
#
# Byte arrays are primarily meant for reading and writing data from/to a stream,
# such as a file or a socket. If you simply want to store a list of numbers,
# you're better off using the `Array` type.
class builtin ByteArray {
  # Returns a new empty `ByteArray`.
  fn pub static new -> Self {
    _INKO.byte_array_allocate
  }

  # Returns a new `ByteArray` created from the given `Array`.
  fn pub static from_array(array: Array[Int]) -> Self {
    let bytes = ByteArray.new

    array.into_iter.each fn (v) { bytes.push(v) }
    bytes
  }

  # Returns a `ByteArray` filled with the given byte.
  #
  # The `times` argument specifies how many times the `with` argument must
  # exist in the byte array.
  #
  # # Examples
  #
  #     let bytes = ByteArray.filled(with: 0, times: 2)
  #
  #     bytes[0] # => 0
  #     bytes[1] # => 0
  fn pub static filled(with: Int, times: Int) -> ByteArray {
    let bytes = new
    let mut index = 0

    while index < times {
      bytes.push(with)

      index += 1
    }

    bytes
  }

  # Removes all values from this `ByteArray`.
  #
  # # Examples
  #
  # Removing all values:
  #
  #     let bytes = ByteArray.from_array([10, 20, 30])
  #
  #     bytes.clear
  #     bytes.length # => 0
  fn pub mut clear {
    # Bytes always fit in a tagged pointer, so there's no need to run any
    # destructors them.
    _INKO.byte_array_clear(self)
  }

  # Appends the bytes of the given `ByteArray` to `self`.
  #
  # # Examples
  #
  #     let a = ByteArray.from_array([10])
  #     let b = ByteArray.from_array([20])
  #
  #     a.append(b)
  #     a # => ByteArray.from_array([10, 20])
  fn pub mut append(other: Self) {
    other.iter.each fn (v) { push(v) }
  }

  # Pushes a value to the back of the `ByteArray`, returning the pushed value.
  #
  # # Examples
  #
  # Pushing a value into a `ByteArray`:
  #
  #     let bytes = ByteArray.new
  #
  #     bytes.push(10) # => 10
  #     bytes.length   # => 1
  fn pub mut push(value: Int) {
    _INKO.byte_array_push(self, value)
    _INKO.moved(value)
  }

  # Removes a value from the back of the `ByteArray`, returning the removed
  # value.
  #
  # If no value was found, a None is returned instead.
  #
  # # Examples
  #
  # Popping an existing value:
  #
  #     let bytes = ByteArray.from_array([10])
  #
  #     bytes.pop    # => Option.Some(10)
  #     bytes.length # => 0
  #
  # Popping a value when the `ByteArray` is empty:
  #
  #     let bytes = ByteArray.new
  #
  #     bytes.pop # => Option.None
  fn pub mut pop -> Option[Int] {
    let val = _INKO.byte_array_pop(self)

    if _INKO.is_undefined(val) { Option.None } else { Option.Some(val) }
  }

  # Removes the value at the given index, returning the removed value.
  #
  # # Examples
  #
  # Removing an existing value:
  #
  #     let bytes = ByteArray.from_array([10])
  #
  #     bytes.remove_at(0) # => 10
  #     bytes.length       # => 0
  #
  # # Panics
  #
  # This method panics if the index is out of bounds.
  fn pub mut remove_at(index: Int) -> Int {
    bounds_check(index, length)
    _INKO.byte_array_remove(self, index)
  }

  # Returns a new `String` using the bytes in this `ByteArray`, draining it in
  # the process.
  #
  # After this method is finished, `self` is left empty. This allows one to
  # convert a temporary `ByteArray` into a `String`, without requiring the list
  # of bytes to be allocated twice.
  #
  # # Examples
  #
  # Draining a `ByteArray` into a `String`:
  #
  #     let bytes = ByteArray.from_array([105, 110, 107, 111])
  #
  #     bytes.drain_to_string # => 'inko'
  #     bytes.empty?          # => True
  fn pub mut drain_to_string -> String {
    _INKO.byte_array_drain_to_string(self)
  }

  # Slices `self` into a new `ByteArray`.
  #
  # Similar to slicing a `String`, slicing a `ByteArray` allows one to extract
  # a sub-array by providing a start position and the number of _bytes_ to
  # include starting at the start position.
  #
  # # Examples
  #
  # Slicing a `ByteArray`:
  #
  #     let bytes = ByteArray.from_array([1, 2, 3, 4])
  #     let sliced = bytes.slice(start: 1, length: 2)
  #
  #     sliced[0] # => 2
  #     sliced[1] # => 3
  fn pub slice(start: Int, length: Int) -> ByteArray {
    bounds_check(start, self.length)

    let new_array = ByteArray.new
    let total_bytes = self.length
    let mut index = start
    let mut end_at = index + length

    if end_at > total_bytes { end_at = total_bytes }

    while index < end_at {
      new_array.push(_INKO.byte_array_get(self, index))

      index += 1
    }

    new_array
  }

  # Returns the byte at the given index, returning None if the index is out of
  # bounds.
  #
  # # Examples
  #
  # Retrieving an existing byte:
  #
  #     let bytes = ByteArray.from_array([10, 20])
  #
  #     bytes.get(0) # => Option.Some(10)
  #
  # Retrieving a non-existing byte:
  #
  #     let bytes = ByteArray.from_array([10, 20])
  #
  #     bytes.get(5) # => Option.None
  fn pub get(index: Int) -> Option[Int] {
    if index < 0 or index >= length { return Option.None }

    Option.Some(_INKO.byte_array_get(self, index))
  }

  # Returns the number of bytes in this `ByteArray`.
  #
  # # Examples
  #
  # Obtaining the length of a `ByteArray`
  #
  #     ByteArray.new.length     # => 0
  #     ByteArray.from_array([10]).length # => 1
  fn pub length -> Int {
    _INKO.byte_array_length(self)
  }

  # Returns `true` if `self` is empty.
  #
  # # Examples
  #
  #     ByteArray.new.empty? => true
  fn pub empty? -> Bool {
    length == 0
  }

  # Converts the `ByteArray` to an `Array!(Int)`.
  #
  # # Examples
  #
  # Converting a `ByteArray`:
  #
  #     let bytes = ByteArray.from_array([105, 110, 107, 111])
  #
  #     bytes.to_array # => [105, 110, 107, 111]
  fn pub to_array -> Array[Int] {
    iter.to_array
  }

  # Returns an `Iter` that iterates over all values in `self`.
  fn pub iter -> Iter[Int, Never] {
    Enum.indexed(length) fn (i) { _INKO.byte_array_get(self, i) }
  }
}

impl Drop for ByteArray {
  fn mut drop {
    let mut index = 0

    while index < length { _INKO.byte_array_get(self, index := index + 1) }

    _INKO.byte_array_drop(self)
  }
}

impl Index[ref Int, Int] for ByteArray {
  # Returns the byte at the given index.
  #
  # # Examples
  #
  # Retrieving an existing byte:
  #
  #     let bytes = ByteArray.from_array([10, 20])
  #
  #     bytes[0] # => 10
  #
  # # Panics
  #
  # This method panics if the index is out of bounds.
  fn pub index(index: ref Int) -> Int {
    bounds_check(index, length)
    _INKO.byte_array_get(self, index)
  }
}

impl SetIndex[ref Int, Int] for ByteArray {
  # Stores a byte at the given index, then returns it.
  #
  # # Examples
  #
  # Setting the value of an existing index:
  #
  #     let bytes = ByteArray.from_array([10, 20])
  #
  #     bytes[0] = 30 # => 30
  #     bytes[0]      # => 30
  #
  # # Panics
  #
  # This method panics if the index is out of bounds.
  fn pub mut set_index(index: ref Int, value: Int) {
    bounds_check(index, length)
    _INKO.byte_array_set(self, index, value)
    _INKO.moved(value)
  }
}

impl ToByteArray for ByteArray {
  fn pub to_byte_array -> Self {
    clone
  }
}

impl IntoByteArray for ByteArray {
  fn pub move into_byte_array -> Self {
    self
  }
}

impl ToString for ByteArray {
  # Returns a new `String` using the bytes in this `ByteArray`.
  #
  # Any invalid UTF-8 sequences will be replaced with `U+FFFD REPLACEMENT
  # CHARACTER`, which looks like this: �
  #
  # # Examples
  #
  # Converting a `ByteArray` into a `String`:
  #
  #     let bytes = ByteArray.from_array([105, 110, 107, 111])
  #
  #     bytes.to_string # => 'inko'
  fn pub to_string -> String {
    _INKO.byte_array_to_string(self)
  }
}

impl IntoString for ByteArray {
  fn pub move into_string -> String {
    drain_to_string
  }
}

impl Equal for ByteArray {
  # Returns `True` if two `ByteArray` objects are equal to each other.
  #
  # Two `ByteArray` objects are considered equal if they have the exact same
  # values in the exact same order.
  #
  # # Examples
  #
  # Comparing two `ByteArray` objects:
  #
  #     ByteArray.from_array([10]) == ByteArray.from_array([10]) # => True
  #     ByteArray.from_array([10]) == ByteArray.from_array([20]) # => False
  fn pub ==(other: ref ByteArray) -> Bool {
    _INKO.byte_array_equals(self, other)
  }
}

impl Clone for ByteArray {
  fn pub clone -> Self {
    _INKO.byte_array_clone(self)
  }
}

impl Hash for ByteArray {
  fn pub hash(hasher: mut Hasher) {
    iter.each fn (v) { hasher.write(v) }
  }
}

impl Contains[Int] for ByteArray {
  fn pub contains?(value: ref Int) -> Bool {
    # Returns `true` if the given byte is contained in `self`.
    #
    # # Examples
    #
    #     let bytes = ByteArray.from_array([10, 20])
    #
    #     bytes.contains?(10) # => true
    iter.any? fn (ours) { ours == value }
  }
}

impl Format for ByteArray {
  fn pub fmt(formatter: mut Formatter) {
    formatter.write('[')

    iter.each_with_index fn (index, byte) {
      if index > 0 { formatter.write(', ') }

      formatter.write(byte.to_string)
    }

    formatter.write(']')
  }
}
